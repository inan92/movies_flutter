import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttermoviesapp/core/rest/movie_service.dart';
import 'package:fluttermoviesapp/features/movie_discover/data/repositories/discover_movie_repository_impl.dart';
import 'package:fluttermoviesapp/features/movies_list/data/repositories/movie_repository_impl.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/discoverbloc/discovermovie_bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/featurebloc/bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/genrebloc/bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/ui/screens/home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  final movieRepository = MovieRepositoryImpl(MovieApiService.create());

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MultiBlocProvider(
      providers: [
        BlocProvider<FeaturedMovieBloc>(
          create: (BuildContext context) =>
              FeaturedMovieBloc(MovieRepositoryImpl(MovieApiService.create())),
        ),
        BlocProvider<GenreBloc>(
          create: (BuildContext context) => GenreBloc(movieRepository),
        ),
        BlocProvider<DiscoverMovieBloc>(
          create: (BuildContext context) => DiscoverMovieBloc(DiscoverMovieRepositoryImpl(MovieApiService.create())),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: HomePage(),
      ),
    );
  }
}
