import 'package:chopper/chopper.dart';
import 'package:fluttermoviesapp/core/util/constants.dart';
import 'package:meta/meta.dart';

part 'movie_service.chopper.dart';

@ChopperApi()
abstract class MovieApiService extends ChopperService {
  static MovieApiService create() {
    final client = ChopperClient(
        baseUrl: BASE_URL,
        services: [_$MovieApiService()],
        converter: JsonConverter());
    return _$MovieApiService(client);
  }

  @Get(path: '/trending/movie/day')
  Future<Response> getFeaturedMovies(@Query('api_key') String appId);

  @Get(path: '/genre/movie/list')
  Future<Response> getGenres(@Query('api_key') String appId);

  @Get(path: '/movie/{movieId}')
  Future<Response> getMovieDetail(
      {@Path("movieId") @required int movieId,
      @Query('api_key') String apiKey});

  @Get(path: '/discover/movie')
  Future<Response> getDiscoverMovies(@Query('api_key') String appId);

  @Get(path: '/search/movie')
  Future<Response> getSearchMovies(@Query('api_key') String apiKey,
      @Query('query') String query, @Query('page') int page);
}
