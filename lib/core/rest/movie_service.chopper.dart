// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_service.dart';

// **************************************************************************
// ChopperGenerator
// **************************************************************************

// ignore_for_file: always_put_control_body_on_new_line, always_specify_types, prefer_const_declarations
class _$MovieApiService extends MovieApiService {
  _$MovieApiService([ChopperClient client]) {
    if (client == null) return;
    this.client = client;
  }

  @override
  final definitionType = MovieApiService;

  @override
  Future<Response<dynamic>> getFeaturedMovies(String appId) {
    final $url = '/trending/movie/day';
    final $params = <String, dynamic>{'api_key': appId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getGenres(String appId) {
    final $url = '/genre/movie/list';
    final $params = <String, dynamic>{'api_key': appId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getMovieDetail({int movieId, String apiKey}) {
    final $url = '/movie/$movieId';
    final $params = <String, dynamic>{'api_key': apiKey};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getDiscoverMovies(String appId) {
    final $url = '/discover/movie';
    final $params = <String, dynamic>{'api_key': appId};
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }

  @override
  Future<Response<dynamic>> getSearchMovies(
      String apiKey, String query, int page) {
    final $url = '/search/movie';
    final $params = <String, dynamic>{
      'api_key': apiKey,
      'query': query,
      'page': page
    };
    final $request = Request('GET', $url, client.baseUrl, parameters: $params);
    return client.send<dynamic, dynamic>($request);
  }
}
