import 'package:chopper/src/response.dart';
import 'package:fluttermoviesapp/core/rest/movie_service.dart';
import 'package:fluttermoviesapp/features/movie_details/domain/repositories/movie_details_repository.dart';

class MovieDetailsRepositoryImpl implements MovieDetailsRepository {
  MovieApiService mService;

  MovieDetailsRepositoryImpl(this.mService);

  @override
  Future<Response> getMovieDetails(int movieId, String apiKey) {

    return mService.getMovieDetail(movieId: movieId, apiKey: apiKey);
  }
}
