/// adult : false
/// backdrop_path : "/pCUdYAaarKqY2AAUtV6xXYO8UGY.jpg"
/// belongs_to_collection : null
/// budget : 63000000
/// genres : [{"id":18,"name":"Drama"}]
/// homepage : "http://www.foxmovies.com/movies/fight-club"
/// id : 550
/// imdb_id : "tt0137523"
/// original_language : "en"
/// original_title : "Fight Club"
/// overview : "A ticking-time-bomb insomniac and a slippery soap salesman channel primal male aggression into a shocking new form of therapy. Their concept catches on, with underground \"fight clubs\" forming in every town, until an eccentric gets in the way and ignites an out-of-control spiral toward oblivion."
/// popularity : 40.797
/// poster_path : "/cRU0PWIGULY4qbwbVFnrBminDv7.jpg"
/// production_companies : [{"id":508,"logo_path":"/7PzJdsLGlR7oW4J0J5Xcd0pHGRg.png","name":"Regency Enterprises","origin_country":"US"},{"id":711,"logo_path":"/tEiIH5QesdheJmDAqQwvtN60727.png","name":"Fox 2000 Pictures","origin_country":"US"},{"id":20555,"logo_path":"/hD8yEGUBlHOcfHYbujp71vD8gZp.png","name":"Taurus Film","origin_country":"DE"},{"id":54051,"logo_path":null,"name":"Atman Entertainment","origin_country":""},{"id":54052,"logo_path":null,"name":"Knickerbocker Films","origin_country":"US"},{"id":25,"logo_path":"/qZCc1lty5FzX30aOCVRBLzaVmcp.png","name":"20th Century Fox","origin_country":"US"},{"id":4700,"logo_path":"/A32wmjrs9Psf4zw0uaixF0GXfxq.png","name":"The Linson Company","origin_country":""}]
/// production_countries : [{"iso_3166_1":"DE","name":"Germany"},{"iso_3166_1":"US","name":"United States of America"}]
/// release_date : "1999-10-15"
/// revenue : 100853753
/// runtime : 139
/// spoken_languages : [{"iso_639_1":"en","name":"English"}]
/// status : "Released"
/// tagline : "Mischief. Mayhem. Soap."
/// title : "Fight Club"
/// video : false
/// vote_average : 8.4
/// vote_count : 18748

class MovieDetails {
  bool _adult;
  String _backdropPath;
  dynamic _belongsToCollection;
  int _budget;
  List<GenresBean> _genres;
  String _homepage;
  int _id;
  String _imdbId;
  String _originalLanguage;
  String _originalTitle;
  String _overview;
  double _popularity;
  String _posterPath;
  List<Production_companiesBean> _productionCompanies;
  List<Production_countriesBean> _productionCountries;
  String _releaseDate;
  int _revenue;
  int _runtime;
  List<Spoken_languagesBean> _spokenLanguages;
  String _status;
  String _tagline;
  String _title;
  bool _video;
  double _voteAverage;
  int _voteCount;

  bool get adult => _adult;
  String get backdropPath => _backdropPath;
  dynamic get belongsToCollection => _belongsToCollection;
  int get budget => _budget;
  List<GenresBean> get genres => _genres;
  String get homepage => _homepage;
  int get id => _id;
  String get imdbId => _imdbId;
  String get originalLanguage => _originalLanguage;
  String get originalTitle => _originalTitle;
  String get overview => _overview;
  double get popularity => _popularity;
  String get posterPath => _posterPath;
  List<Production_companiesBean> get productionCompanies => _productionCompanies;
  List<Production_countriesBean> get productionCountries => _productionCountries;
  String get releaseDate => _releaseDate;
  int get revenue => _revenue;
  int get runtime => _runtime;
  List<Spoken_languagesBean> get spokenLanguages => _spokenLanguages;
  String get status => _status;
  String get tagline => _tagline;
  String get title => _title;
  bool get video => _video;
  double get voteAverage => _voteAverage;
  int get voteCount => _voteCount;

  MovieDetails(this._adult, this._backdropPath, this._belongsToCollection, this._budget, this._genres, this._homepage, this._id, this._imdbId, this._originalLanguage, this._originalTitle, this._overview, this._popularity, this._posterPath, this._productionCompanies, this._productionCountries, this._releaseDate, this._revenue, this._runtime, this._spokenLanguages, this._status, this._tagline, this._title, this._video, this._voteAverage, this._voteCount);

  MovieDetails.map(Map<String,dynamic> obj) {
    this._adult = obj["adult"];
    this._backdropPath = obj["backdropPath"];
    this._belongsToCollection = obj["belongsToCollection"];
    this._budget = obj["budget"];
    if (obj['genres'] != null) {
      this._genres = new List<GenresBean>();
      obj['genres'].forEach((v) {
        this._genres.add(new GenresBean.map(v));
      });
    }
    this._homepage = obj["homepage"];
    this._id = obj["id"];
    this._imdbId = obj["imdbId"];
    this._originalLanguage = obj["originalLanguage"];
    this._originalTitle = obj["originalTitle"];
    this._overview = obj["overview"];
    this._popularity = obj["popularity"];
    this._posterPath = obj["posterPath"];
    this._productionCompanies = obj["productionCompanies"];
    this._productionCountries = obj["productionCountries"];
    this._releaseDate = obj["releaseDate"];
    this._revenue = obj["revenue"];
    this._runtime = obj["runtime"];
    this._spokenLanguages = obj["spokenLanguages"];
    this._status = obj["status"];
    this._tagline = obj["tagline"];
    this._title = obj["title"];
    this._video = obj["video"];
    this._voteAverage = obj["voteAverage"];
    this._voteCount = obj["voteCount"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["adult"] = _adult;
    map["backdropPath"] = _backdropPath;
    map["belongsToCollection"] = _belongsToCollection;
    map["budget"] = _budget;
    map["genres"] = _genres;
    map["homepage"] = _homepage;
    map["id"] = _id;
    map["imdbId"] = _imdbId;
    map["originalLanguage"] = _originalLanguage;
    map["originalTitle"] = _originalTitle;
    map["overview"] = _overview;
    map["popularity"] = _popularity;
    map["posterPath"] = _posterPath;
    map["productionCompanies"] = _productionCompanies;
    map["productionCountries"] = _productionCountries;
    map["releaseDate"] = _releaseDate;
    map["revenue"] = _revenue;
    map["runtime"] = _runtime;
    map["spokenLanguages"] = _spokenLanguages;
    map["status"] = _status;
    map["tagline"] = _tagline;
    map["title"] = _title;
    map["video"] = _video;
    map["voteAverage"] = _voteAverage;
    map["voteCount"] = _voteCount;
    return map;
  }

}

/// iso_639_1 : "en"
/// name : "English"

class Spoken_languagesBean {
  String _iso6391;
  String _name;

  String get iso6391 => _iso6391;
  String get name => _name;

  Spoken_languagesBean(this._iso6391, this._name);

  Spoken_languagesBean.map(dynamic obj) {
    this._iso6391 = obj["iso6391"];
    this._name = obj["name"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["iso6391"] = _iso6391;
    map["name"] = _name;
    return map;
  }

}

/// iso_3166_1 : "DE"
/// name : "Germany"

class Production_countriesBean {
  String _iso31661;
  String _name;

  String get iso31661 => _iso31661;
  String get name => _name;

  Production_countriesBean(this._iso31661, this._name);

  Production_countriesBean.map(dynamic obj) {
    this._iso31661 = obj["iso31661"];
    this._name = obj["name"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["iso31661"] = _iso31661;
    map["name"] = _name;
    return map;
  }

}

/// id : 508
/// logo_path : "/7PzJdsLGlR7oW4J0J5Xcd0pHGRg.png"
/// name : "Regency Enterprises"
/// origin_country : "US"

class Production_companiesBean {
  int _id;
  String _logoPath;
  String _name;
  String _originCountry;

  int get id => _id;
  String get logoPath => _logoPath;
  String get name => _name;
  String get originCountry => _originCountry;

  Production_companiesBean(this._id, this._logoPath, this._name, this._originCountry);

  Production_companiesBean.map(dynamic obj) {
    this._id = obj["id"];
    this._logoPath = obj["logoPath"];
    this._name = obj["name"];
    this._originCountry = obj["originCountry"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["logoPath"] = _logoPath;
    map["name"] = _name;
    map["originCountry"] = _originCountry;
    return map;
  }

}

/// id : 18
/// name : "Drama"

class GenresBean {
  int _id;
  String _name;

  int get id => _id;
  String get name => _name;

  GenresBean(this._id, this._name);

  GenresBean.map(dynamic obj) {
    this._id = obj["id"];
    this._name = obj["name"];
  }

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["name"] = _name;
    return map;
  }

}