import 'package:equatable/equatable.dart';

abstract class MovieDetailsEvent extends Equatable {
  const MovieDetailsEvent();
}

class GetMovieDetails extends MovieDetailsEvent {
  final String apiKey;
  final int movieId;

  const GetMovieDetails(this.movieId, this.apiKey);

  @override
  // TODO: implement props
  List<Object> get props => [this.movieId,this.apiKey];
}
