import 'package:equatable/equatable.dart';
import 'package:fluttermoviesapp/features/movie_details/data/models/movie_details.dart';

abstract class MovieDetailsState extends Equatable {
  const MovieDetailsState();
}

class MovieDetailInitial extends MovieDetailsState {
  @override
  List<Object> get props => [];
}

class MovieDetailLoading extends MovieDetailsState {
  const MovieDetailLoading();

  @override
  List<Object> get props => [];
}

class MovieDetailsLoaded extends MovieDetailsState {
  final MovieDetailsResponse movieDetailsResponse;

  const MovieDetailsLoaded(this.movieDetailsResponse);

  @override
  // TODO: implement props
  List<Object> get props => [movieDetailsResponse];
}

class MovieDetailsError extends MovieDetailsState{
  final String errorMessage;

  MovieDetailsError(this.errorMessage);

  @override
  // TODO: implement props
  List<Object> get props => [this.errorMessage];

}
