import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:fluttermoviesapp/core/util/constants.dart';
import 'package:fluttermoviesapp/features/movie_details/data/models/movie_details.dart';
import 'package:fluttermoviesapp/features/movie_details/domain/repositories/movie_details_repository.dart';

import './bloc.dart';

class MovieDetailsBloc extends Bloc<MovieDetailsEvent, MovieDetailsState> {
  final MovieDetailsRepository movieRepository;
  final int movieId;

  MovieDetailsBloc(this.movieRepository, this.movieId);

  @override
  MovieDetailsState get initialState => MovieDetailInitial();

  @override
  Stream<MovieDetailsState> mapEventToState(
    MovieDetailsEvent event,
  ) async* {
    // TODO: Add Logic
    print("$event $movieId");
    yield MovieDetailLoading();

    if (event is GetMovieDetails) {
      try {
        final mResponse =
            await movieRepository.getMovieDetails(movieId, API_KEY);
        print(mResponse.bodyString);
        Map<String, dynamic> map = json.decode(mResponse.bodyString);
        MovieDetailsResponse movieResponse = MovieDetailsResponse.fromJson(map);
        yield MovieDetailsLoaded(movieResponse);
      } catch (e) {
        print(e);
        yield MovieDetailsError("Sorry, couldn't find any information about this movie");
      }
    }
  }
}
