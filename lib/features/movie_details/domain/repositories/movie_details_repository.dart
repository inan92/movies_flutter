import 'package:chopper/chopper.dart';

abstract class MovieDetailsRepository {
  Future<Response> getMovieDetails(int movieId, String apiKey);
}
