import 'package:chopper/chopper.dart';

abstract class DiscoverMovieRepository{
  Future<Response> getDiscoverMovies(String apiKey);
}