/// page : 1
/// total_results : 10000
/// total_pages : 500
/// results : [{"popularity":565.041,"vote_count":2896,"video":false,"poster_path":"/xBHvZcjRiWyobQ9kxBhO6B2dtRI.jpg","id":419704,"adult":false,"backdrop_path":"/5BwqwxMEjeFtdknRV792Svo0K1v.jpg","original_language":"en","original_title":"Ad Astra","genre_ids":[18,878],"title":"Ad Astra","vote_average":5.9,"overview":"The near future, a time when both hope and hardships drive humanity to look to the stars and beyond. While a mysterious phenomenon menaces to destroy life on planet Earth, astronaut Roy McBride undertakes a mission across the immensity of space and its many perils to uncover the truth about a lost expedition that decades before boldly faced emptiness and silence in search of the unknown.","release_date":"2019-09-17"},{"popularity":362.953,"vote_count":2711,"video":false,"poster_path":"/y95lQLnuNKdPAzw9F9Ab8kJ80c3.jpg","id":38700,"adult":false,"backdrop_path":"/upUy2QhMZEmtypPW3PdieKLAHxh.jpg","original_language":"en","original_title":"Bad Boys for Life","genre_ids":[28,80,53],"title":"Bad Boys for Life","vote_average":7.1,"overview":"Marcus and Mike are forced to confront new threats, career changes, and midlife crises as they join the newly created elite team AMMO of the Miami police department to take down the ruthless Armando Armas, the vicious leader of a Miami drug cartel.","release_date":"2020-01-15"},{"popularity":269.324,"vote_count":2293,"video":false,"poster_path":"/aQvJ5WPzZgYVDrxLX4R6cLJCEaQ.jpg","id":454626,"adult":false,"backdrop_path":"/stmYfCUGd8Iy6kAMBr6AmWqx8Bq.jpg","original_language":"en","original_title":"Sonic the Hedgehog","genre_ids":[28,35,878,10751],"title":"Sonic the Hedgehog","vote_average":7.5,"overview":"Based on the global blockbuster videogame franchise from Sega, Sonic the Hedgehog tells the story of the world’s speediest hedgehog as he embraces his new home on Earth. In this live-action adventure comedy, Sonic and his new best friend team up to defend the planet from the evil genius Dr. Robotnik and his plans for world domination.","release_date":"2020-02-12"},{"popularity":252.088,"vote_count":3863,"video":false,"poster_path":"/db32LaOibwEliAmSL2jjDF6oDdj.jpg","id":181812,"adult":false,"backdrop_path":"/jOzrELAzFxtMx2I4uDGHOotdfsS.jpg","original_language":"en","original_title":"Star Wars: The Rise of Skywalker","genre_ids":[28,12,878],"title":"Star Wars: The Rise of Skywalker","vote_average":6.5,"overview":"The surviving Resistance faces the First Order once again as the journey of Rey, Finn and Poe Dameron continues. With the power and knowledge of generations behind them, the final battle begins.","release_date":"2019-12-18"},{"popularity":244.021,"vote_count":1446,"video":false,"poster_path":"/8WUVHemHFH2ZIP6NWkwlHWsyrEL.jpg","id":338762,"adult":false,"backdrop_path":"/ocUrMYbdjknu2TwzMHKT9PBBQRw.jpg","original_language":"en","original_title":"Bloodshot","genre_ids":[28,878],"title":"Bloodshot","vote_average":7.2,"overview":"After he and his wife are murdered, marine Ray Garrison is resurrected by a team of scientists. Enhanced with nanotechnology, he becomes a superhuman, biotech killing machine—'Bloodshot'. As Ray first trains with fellow super-soldiers, he cannot recall anything from his former life. But when his memories flood back and he remembers the man that killed both him and his wife, he breaks out of the facility to get revenge, only to discover that there's more to the conspiracy than he thought.","release_date":"2020-03-05"},{"popularity":213.655,"id":495764,"video":false,"vote_count":2758,"vote_average":7.2,"title":"Birds of Prey (and the Fantabulous Emancipation of One Harley Quinn)","release_date":"2020-02-05","original_language":"en","original_title":"Birds of Prey (and the Fantabulous Emancipation of One Harley Quinn)","genre_ids":[28,80,35],"backdrop_path":"/jiqD14fg7UTZOT6qgvzTmfRYpWI.jpg","adult":false,"overview":"Harley Quinn joins forces with a singer, an assassin and a police detective to help a young girl who had a hit placed on her after she stole a rare diamond from a crime lord.","poster_path":"/h4VB6m0RwcicVEZvzftYZyKXs6K.jpg"},{"popularity":210.778,"vote_count":61,"video":false,"poster_path":"/7W0G3YECgDAfnuiHG91r8WqgIOe.jpg","id":446893,"adult":false,"backdrop_path":"/qsxhnirlp7y4Ae9bd11oYJSX59j.jpg","original_language":"en","original_title":"Trolls World Tour","genre_ids":[12,16,35,14,10402,10751],"title":"Trolls World Tour","vote_average":8.1,"overview":"Queen Poppy and Branch make a surprising discovery — there are other Troll worlds beyond their own, and their distinct differences create big clashes between these various tribes. When a mysterious threat puts all of the Trolls across the land in danger, Poppy, Branch, and their band of friends must embark on an epic quest to create harmony among the feuding Trolls to unite them against certain doom.","release_date":"2020-03-12"},{"popularity":184.412,"vote_count":797,"video":false,"poster_path":"/z4A6mFOLTMZAhCSPRyrtzG0SPbd.jpg","id":475303,"adult":false,"backdrop_path":"/6fkqwqLEcDZOEAnBBfKAniwNxtx.jpg","original_language":"en","original_title":"A Rainy Day in New York","genre_ids":[35,10749],"title":"A Rainy Day in New York","vote_average":6.6,"overview":"Two young people arrive in New York to spend a weekend, but once they arrive they're met with bad weather and a series of adventures.","release_date":"2019-07-26"},{"popularity":184.012,"vote_count":770,"video":false,"poster_path":"/jtrhTYB7xSrJxR1vusu99nvnZ1g.jpg","id":522627,"adult":false,"backdrop_path":"/9Qfawg9WT3cSbBXQgDRuWbYS9lj.jpg","original_language":"en","original_title":"The Gentlemen","genre_ids":[28,35,80],"title":"The Gentlemen","vote_average":7.8,"overview":"American expat Mickey Pearson has built a highly profitable marijuana empire in London. When word gets out that he’s looking to cash out of the business forever it triggers plots, schemes, bribery and blackmail in an attempt to steal his domain out from under him.","release_date":"2019-12-16"},{"popularity":161.06,"vote_count":2095,"video":false,"poster_path":"/8ZX18L5m6rH5viSYpRnTSbb9eXh.jpg","id":619264,"adult":false,"backdrop_path":"/3tkDMNfM2YuIAJlvGO6rfIzAnfG.jpg","original_language":"es","original_title":"El hoyo","genre_ids":[18,878],"title":"The Platform","vote_average":7.1,"overview":"A mysterious place, an indescribable prison, a deep hole. An unknown number of levels. Two inmates living on each level. A descending platform containing food for all of them. An inhuman fight for survival, but also an opportunity for solidarity…","release_date":"2019-11-08"},{"popularity":147.747,"vote_count":1039,"video":false,"poster_path":"/f4aul3FyD3jv3v4bul1IrkWZvzq.jpg","id":508439,"adult":false,"backdrop_path":"/xFxk4vnirOtUxpOEWgA1MCRfy6J.jpg","original_language":"en","original_title":"Onward","genre_ids":[12,16,35,14,10751],"title":"Onward","vote_average":8,"overview":"In a suburban fantasy world, two teenage elf brothers embark on an extraordinary quest to discover if there is still a little magic left out there.","release_date":"2020-02-29"},{"popularity":145.228,"id":16119,"video":false,"vote_count":725,"vote_average":6.3,"title":"Cinderella III: A Twist in Time","release_date":"2007-02-06","original_language":"en","original_title":"Cinderella III: A Twist in Time","genre_ids":[16,10749,10751,14],"backdrop_path":"/8IresDDceExrC67l7MxDOcITJTm.jpg","adult":false,"overview":"When Lady Tremaine steals the Fairy Godmother's wand and changes history, it's up to Cinderella to restore the timeline and reclaim her prince.","poster_path":"/98PHWv6TTpjxvs6EHKJVyrjuuus.jpg"},{"popularity":144.513,"vote_count":3043,"video":false,"poster_path":"/bB42KDdfWkOvmzmYkmK58ZlCa9P.jpg","id":512200,"adult":false,"backdrop_path":"/oLma4sWjqlXVr0E3jpaXQCytuG9.jpg","original_language":"en","original_title":"Jumanji: The Next Level","genre_ids":[12,35,14],"title":"Jumanji: The Next Level","vote_average":6.8,"overview":"As the gang return to Jumanji to rescue one of their own, they discover that nothing is as they expect. The players will have to brave parts unknown and unexplored in order to escape the world’s most dangerous game.","release_date":"2019-12-04"},{"popularity":143.604,"vote_count":1323,"video":false,"poster_path":"/5EufsDwXdY2CVttYOk2WtYhgKpa.jpg","id":570670,"adult":false,"backdrop_path":"/uZMZyvarQuXLRqf3xdpdMqzdtjb.jpg","original_language":"en","original_title":"The Invisible Man","genre_ids":[27,878,53],"title":"The Invisible Man","vote_average":7.1,"overview":"When Cecilia's abusive ex takes his own life and leaves her his fortune, she suspects his death was a hoax. As a series of coincidences turn lethal, Cecilia works to prove that she is being hunted by someone nobody can see.","release_date":"2020-02-26"},{"popularity":138.488,"vote_count":805,"video":false,"poster_path":"/fV8li11x11apH0BtToWvMFrrc99.jpg","id":7459,"adult":false,"backdrop_path":"/4lXZkIp40nwcxEYhrCCGjs0qUXw.jpg","original_language":"en","original_title":"Speed Racer","genre_ids":[28,12,35,10751],"title":"Speed Racer","vote_average":5.9,"overview":"Speed Racer is the tale of a young and brilliant racing driver. When corruption in the racing leagues costs his brother his life, he must team up with the police and the mysterious Racer X to bring an end to the corruption and criminal activities. Inspired by the cartoon series.","release_date":"2008-05-07"},{"popularity":135.156,"vote_count":5843,"video":false,"poster_path":"/uuitWHpJwxD1wruFl2nZHIb4UGN.jpg","id":772,"adult":false,"backdrop_path":"/1uHTuwx5h9T3XzsXijMMKybDFvZ.jpg","original_language":"en","original_title":"Home Alone 2: Lost in New York","genre_ids":[12,35,80,10751],"title":"Home Alone 2: Lost in New York","vote_average":6.6,"overview":"Instead of flying to Florida with his folks, Kevin ends up alone in New York, where he gets a hotel room with his dad's credit card—despite problems from a clerk and meddling bellboy. But when Kevin runs into his old nemeses, the Wet Bandits, he's determined to foil their plans to rob a toy store on Christmas eve.","release_date":"1992-11-19"},{"popularity":128.418,"vote_count":134,"video":false,"poster_path":"/z616JJQxshPK1VG4E2sV1qcVyf6.jpg","id":19543,"adult":false,"backdrop_path":null,"original_language":"en","original_title":"Anacondas: Trail of Blood","genre_ids":[28,27,878],"title":"Anacondas: Trail of Blood","vote_average":4.2,"overview":"A genetically created Anaconda, cut in half, regenerates itself into two aggressive giant snakes, due to the Blood Orchid.","release_date":"2009-02-27"},{"popularity":126.37,"id":614375,"video":false,"vote_count":6,"vote_average":7.9,"title":"F#*@BOIS","release_date":"2019-08-02","original_language":"tl","original_title":"F#*@BOIS","genre_ids":[18],"backdrop_path":null,"adult":false,"overview":"Ace, 23, and Miko, 17, desperately want to become famous actors but it seems the Universe has a different plan for their lives.","poster_path":"/hjOX3eTRR37WdsTMXOcD0gF9BA7.jpg"},{"popularity":124.019,"vote_count":11070,"video":false,"poster_path":"/udDclJoHjfjb8Ekgsd4FDteOkCU.jpg","id":475557,"adult":false,"backdrop_path":"/f5F4cRhQdUbyVbB5lTNCwUzD6BP.jpg","original_language":"en","original_title":"Joker","genre_ids":[80,18,53],"title":"Joker","vote_average":8.2,"overview":"During the 1980s, a failed stand-up comedian is driven insane and turns to a life of crime and chaos in Gotham City while becoming an infamous psychopathic crime figure.","release_date":"2019-10-02"},{"popularity":113.485,"vote_count":9016,"video":false,"poster_path":"/qa6HCwP4Z15l3hpsASz3auugEW6.jpg","id":920,"adult":false,"backdrop_path":"/8KeWhoMpqbzZRyHPkTtWSLWkL5L.jpg","original_language":"en","original_title":"Cars","genre_ids":[12,16,35,10751],"title":"Cars","vote_average":6.8,"overview":"Lightning McQueen, a hotshot rookie race car driven to succeed, discovers that life is about the journey, not the finish line, when he finds himself unexpectedly detoured in the sleepy Route 66 town of Radiator Springs. On route across the country to the big Piston Cup Championship in California to compete against two seasoned pros, McQueen gets to know the town's offbeat characters.","release_date":"2006-06-08"}]

class DiscoverMovies {
  int page;
  int totalResults;
  int totalPages;
  List<ResultsBean> results;

  DiscoverMovies({this.page, this.totalResults, this.totalPages, this.results});

  DiscoverMovies.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    totalResults = json['total_results'];
    totalPages = json['total_pages'];
    if (json['results'] != null) {
      results = new List<ResultsBean>();
      json['results'].forEach((v) {
        results.add(new ResultsBean.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['total_results'] = this.totalResults;
    data['total_pages'] = this.totalPages;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

/// popularity : 565.041
/// vote_count : 2896
/// video : false
/// poster_path : "/xBHvZcjRiWyobQ9kxBhO6B2dtRI.jpg"
/// id : 419704
/// adult : false
/// backdrop_path : "/5BwqwxMEjeFtdknRV792Svo0K1v.jpg"
/// original_language : "en"
/// original_title : "Ad Astra"
/// genre_ids : [18,878]
/// title : "Ad Astra"
/// vote_average : 5.9
/// overview : "The near future, a time when both hope and hardships drive humanity to look to the stars and beyond. While a mysterious phenomenon menaces to destroy life on planet Earth, astronaut Roy McBride undertakes a mission across the immensity of space and its many perils to uncover the truth about a lost expedition that decades before boldly faced emptiness and silence in search of the unknown."
/// release_date : "2019-09-17"

class ResultsBean {
  double _popularity;
  int _voteCount;
  bool _video;
  String _posterPath;
  int _id;
  bool _adult;
  String _backdropPath;
  String _originalLanguage;
  String _originalTitle;
  List<int> _genreIds;
  String _title;
  double _voteAverage;
  String _overview;
  String _releaseDate;


  double get popularity => _popularity;
  int get voteCount => _voteCount;
  bool get video => _video;
  String get posterPath => _posterPath;
  int get id => _id;
  bool get adult => _adult;
  String get backdropPath => _backdropPath;
  String get originalLanguage => _originalLanguage;
  String get originalTitle => _originalTitle;
  List<int> get genreIds => _genreIds;
  String get title => _title;
  double get voteAverage => _voteAverage;
  String get overview => _overview;
  String get releaseDate => _releaseDate;

  ResultsBean(this._popularity, this._voteCount, this._video, this._posterPath, this._id, this._adult, this._backdropPath, this._originalLanguage, this._originalTitle, this._genreIds, this._title, this._voteAverage, this._overview, this._releaseDate);

  ResultsBean.fromJson(Map<String, dynamic> json) {
    _popularity = json['popularity'];
    _voteCount = json['vote_count'];
    _video = json['video'];
    _posterPath = json['poster_path'];
    _id = json['id'];
    _adult = json['adult'];
    _backdropPath = json['backdrop_path'];
    _originalLanguage = json['original_language'];
    _originalTitle = json['original_title'];
    _genreIds = json['genre_ids'].cast<int>();
    _title = json['title'];

    _voteAverage = json['vote_average']+0.0;
    _overview = json['overview'];
    _releaseDate = json['release_date'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['popularity'] = this._popularity;
    data['vote_count'] = this._voteCount;
    data['video'] = this._video;
    data['poster_path'] = this._posterPath;
    data['id'] = this._id;
    data['adult'] = this._adult;
    data['backdrop_path'] = this._backdropPath;
    data['original_language'] = this._originalLanguage;
    data['original_title'] = this._originalTitle;
    data['genre_ids'] = this._genreIds;
    data['title'] = this._title;
    data['vote_average'] = this._voteAverage;
    data['overview'] = this._overview;
    data['release_date'] = this._releaseDate;
    return data;
  }
  @override
  String toString() {
    // TODO: implement toString
    return "$_posterPath $_backdropPath $_originalTitle";
  }
}