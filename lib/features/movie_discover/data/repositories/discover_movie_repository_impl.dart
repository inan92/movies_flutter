import 'package:chopper/src/response.dart';
import 'package:fluttermoviesapp/core/rest/movie_service.dart';
import 'package:fluttermoviesapp/features/movie_discover/domain/repositories/discover_movie_repository.dart';

class DiscoverMovieRepositoryImpl implements DiscoverMovieRepository {
  MovieApiService mService;

  DiscoverMovieRepositoryImpl(this.mService);

  @override
  Future<Response> getDiscoverMovies(String apiKey) {
    // TODO: implement getDiscoverMovies
    return mService.getDiscoverMovies(apiKey);
  }
}
