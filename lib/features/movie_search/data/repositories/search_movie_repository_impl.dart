import 'package:chopper/src/response.dart';
import 'package:fluttermoviesapp/core/rest/movie_service.dart';
import 'package:fluttermoviesapp/features/movie_search/domain/search_movie_repository.dart';

class SearchMovieRepositoryImpl implements SearchMovieRepository {
  MovieApiService mService;

  SearchMovieRepositoryImpl(this.mService);

  @override
  Future<Response> getSearchMovies(String apiKey, String query, int page) {
    return mService.getSearchMovies(apiKey, query, page);
  }
}
