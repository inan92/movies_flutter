import 'package:equatable/equatable.dart';
import 'package:fluttermoviesapp/features/movie_discover/data/models/discover_movies.dart';

class SearchModel extends Equatable {
  int page;
  int totalResults;
  int totalPages;
  List<ResultsBean> results;

  SearchModel({this.page, this.totalResults, this.totalPages, this.results});

  SearchModel.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    totalResults = json['total_results'];
    totalPages = json['total_pages'];
    if (json['results'] != null) {
      results = new List<ResultsBean>();
      json['results'].forEach((v) {
        results.add(new ResultsBean.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['total_results'] = this.totalResults;
    data['total_pages'] = this.totalPages;
    if (this.results != null) {
      data['results'] = this.results.map((v) => v.toJson()).toList();
    }
    return data;
  }

  @override
  // TODO: implement props
  List<Object> get props => [page, totalPages, totalResults, results];

  @override
  String toString() => "Search page {$page} total results {$totalResults}";
}
