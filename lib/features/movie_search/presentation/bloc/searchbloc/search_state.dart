import 'package:equatable/equatable.dart';
import 'package:fluttermoviesapp/features/movie_search/data/models/search_model.dart';

abstract class SearchState extends Equatable {
  const SearchState();
}

class SearchInitial extends SearchState {
  const SearchInitial();

  @override
  List<Object> get props => [];
}

class SearchLoading extends SearchState {
  const SearchLoading();

  @override
  List<Object> get props => [];
}

class SearchLoaded extends SearchState {
  final SearchModel searchModel;
  final bool hasReachedMax;

  SearchLoaded({
    this.searchModel,
    this.hasReachedMax,
  });

  SearchLoaded copyWith({
    SearchModel searchModel,
    bool hasReachedMax,
  }) {
    return SearchLoaded(
      searchModel: searchModel ?? this.searchModel,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  // TODO: implement props
  List<Object> get props => [searchModel];
}
