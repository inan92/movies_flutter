import 'package:equatable/equatable.dart';

abstract class SearchEvent extends Equatable {
  const SearchEvent();
}

class GetSearchedMovies extends SearchEvent{

  @override
  // TODO: implement props
  List<Object> get props => null;

  @override
  String toString() => 'Search';
}
