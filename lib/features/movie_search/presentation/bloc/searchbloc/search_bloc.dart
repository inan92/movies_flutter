import 'dart:async';
import 'package:bloc/bloc.dart';
import './bloc.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  @override
  SearchState get initialState => SearchInitial();

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    // TODO: Add Logic
    if(event is SearchEvent && !_hasReachedMax(state)){
      try{
        if(state is SearchInitial){

        }
      }catch(e){
        print(e.toString());
      }
    }
  }

  bool _hasReachedMax(SearchState state) =>
      state is SearchLoaded && state.hasReachedMax;
}
