import 'package:chopper/chopper.dart';

abstract class SearchMovieRepository{
  Future<Response> getSearchMovies(String apiKey, String query, int page);
}