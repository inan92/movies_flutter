import 'package:equatable/equatable.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/featured_movies.dart';

abstract class FeaturedMovieState extends Equatable {
  const FeaturedMovieState();
}

class FeaturedMovieInitial extends FeaturedMovieState {
  const FeaturedMovieInitial();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class FeaturedMovieLoading extends FeaturedMovieState {
  const FeaturedMovieLoading();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class FeaturedMovieLoaded extends FeaturedMovieState {
  final FeaturedMovieResponse featuredMovieResponse;

  const FeaturedMovieLoaded(this.featuredMovieResponse);

  @override
  // TODO: implement props
  List<Object> get props => [featuredMovieResponse];
}
