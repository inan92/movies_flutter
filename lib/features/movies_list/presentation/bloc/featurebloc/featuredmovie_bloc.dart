import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:fluttermoviesapp/core/util/constants.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/featured_movies.dart';
import 'package:fluttermoviesapp/features/movies_list/domain/repositories/movie_repository.dart';

import './bloc.dart';

class FeaturedMovieBloc extends Bloc<FeaturedMovieEvent, FeaturedMovieState> {
  final MovieRepository movieRepository;

  FeaturedMovieBloc(this.movieRepository);

  @override
  FeaturedMovieState get initialState => FeaturedMovieInitial();

  @override
  Stream<FeaturedMovieState> mapEventToState(
    FeaturedMovieEvent event,
  ) async* {
    // TODO: Add Logic
    yield FeaturedMovieLoading();
    print(event);
    if (event is GetFeaturedMovies) {
      try {
        final mResponse = await movieRepository.getFeatureMovies(API_KEY);
        Map<String, dynamic> map = json.decode(mResponse.bodyString);
        FeaturedMovieResponse movieResponse =
            FeaturedMovieResponse.fromJson(map);
        yield FeaturedMovieLoaded(movieResponse);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
