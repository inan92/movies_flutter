import 'package:equatable/equatable.dart';

abstract class FeaturedMovieEvent extends Equatable {
  const FeaturedMovieEvent();
}

class GetFeaturedMovies extends FeaturedMovieEvent {
  final String apiKey;

  const GetFeaturedMovies(this.apiKey);

  @override
  // TODO: implement props
  List<Object> get props => [this.apiKey];
}
