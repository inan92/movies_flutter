import 'package:equatable/equatable.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/genres.dart';

abstract class GenreState extends Equatable {
  const GenreState();
}

class GenreInitial extends GenreState {
  const GenreInitial();

  @override
  List<Object> get props => [];
}

class GenreLoading extends GenreState {
  const GenreLoading();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class GenreLoaded extends GenreState {
  final GenreResponse genreResponse;

  const GenreLoaded(this.genreResponse);

  @override
  // TODO: implement props
  List<Object> get props => [genreResponse];
}
