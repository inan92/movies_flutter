import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:fluttermoviesapp/core/util/constants.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/genres.dart';
import 'package:fluttermoviesapp/features/movies_list/domain/repositories/movie_repository.dart';

import './bloc.dart';

class GenreBloc extends Bloc<GenreEvent, GenreState> {
  final MovieRepository movieRepository;

  GenreBloc(this.movieRepository);

  @override
  GenreState get initialState => GenreInitial();

  @override
  Stream<GenreState> mapEventToState(
    GenreEvent event,
  ) async* {
    // TODO: Add Logic
    yield GenreLoading();
    final mResponse = await movieRepository.getGenreList(API_KEY);

    Map<String, dynamic> map = json.decode(mResponse.bodyString);
    GenreResponse genreResponse = GenreResponse.fromJson(map);
    yield GenreLoaded(genreResponse);
  }
}
