import 'package:equatable/equatable.dart';

abstract class GenreEvent extends Equatable {
  const GenreEvent();
}

class GetGenres extends GenreEvent {
  final String apiKey;

  const GetGenres(this.apiKey);

  @override
  // TODO: implement props
  List<Object> get props => [this.apiKey];
}
