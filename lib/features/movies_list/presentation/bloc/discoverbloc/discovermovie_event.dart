import 'package:equatable/equatable.dart';

abstract class DiscovermovieEvent extends Equatable {
  const DiscovermovieEvent();
}

class GetDiscoverMoviesEvent extends DiscovermovieEvent{
  final String apiKey;

  const GetDiscoverMoviesEvent(this.apiKey);

  @override
  // TODO: implement props
  List<Object> get props => [this.apiKey];

}