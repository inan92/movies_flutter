import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:fluttermoviesapp/core/util/constants.dart';
import 'package:fluttermoviesapp/features/movie_discover/data/models/discover_movies.dart';
import 'package:fluttermoviesapp/features/movie_discover/domain/repositories/discover_movie_repository.dart';

import './bloc.dart';

class DiscoverMovieBloc extends Bloc<DiscovermovieEvent, DiscovermovieState> {
  final DiscoverMovieRepository movieRepository;

  DiscoverMovieBloc(this.movieRepository);

  @override
  DiscovermovieState get initialState => DiscoverMovieInitial();

  @override
  Stream<DiscovermovieState> mapEventToState(
    DiscovermovieEvent event,
  ) async* {
    // TODO: Add Logic
    yield DiscoverMovieLoading();
    if (event is GetDiscoverMoviesEvent) {
      try {
        final mResponse = await movieRepository.getDiscoverMovies(API_KEY);
        Map<String, dynamic> map = json.decode(mResponse.bodyString);
        DiscoverMovies movieResponse = DiscoverMovies.fromJson(map);
        yield DiscoverMovieLoaded(movieResponse);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}
