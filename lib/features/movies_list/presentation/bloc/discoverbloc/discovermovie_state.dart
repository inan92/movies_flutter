import 'package:equatable/equatable.dart';
import 'package:fluttermoviesapp/features/movie_discover/data/models/discover_movies.dart';

abstract class DiscovermovieState extends Equatable {
  const DiscovermovieState();
}

class DiscoverMovieInitial extends DiscovermovieState {

  const DiscoverMovieInitial();
  @override
  List<Object> get props => [];
}

class DiscoverMovieLoading extends DiscovermovieState{
  const DiscoverMovieLoading();

  @override
  // TODO: implement props
  List<Object> get props => [];
}

class DiscoverMovieLoaded extends DiscovermovieState{
  final DiscoverMovies discoverMovies;

  DiscoverMovieLoaded(this.discoverMovies);

  @override
  // TODO: implement props
  List<Object> get props => [discoverMovies];

}
