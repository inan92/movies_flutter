import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttermoviesapp/core/util/constants.dart';
import 'package:fluttermoviesapp/features/movie_discover/data/models/discover_movies.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/featured_movies.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/genres.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/discoverbloc/discovermovie_bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/discoverbloc/discovermovie_event.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/discoverbloc/discovermovie_state.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/featurebloc/bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/featurebloc/featuredmovie_bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/featurebloc/featuredmovie_state.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/bloc/genrebloc/bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/ui/widgets/genre_container.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/ui/widgets/home_page_featured_widget.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/ui/widgets/movie_container.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/ui/widgets/section_container.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeState();
  }
}

class HomeState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    BlocProvider.of<FeaturedMovieBloc>(context).add(GetFeaturedMovies(API_KEY));
    BlocProvider.of<GenreBloc>(context).add(GetGenres(API_KEY));
    BlocProvider.of<DiscoverMovieBloc>(context)
        .add(GetDiscoverMoviesEvent(API_KEY));

    return SafeArea(
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: Colors.white,
            elevation: 0,
            centerTitle: true,
            title: Text(
              'Movies',
              style: TextStyle(color: Colors.red, fontWeight: FontWeight.bold),
            ),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right:16.0),
                child: InkWell(
                  onTap: (){

                  },
                  child: Icon(
                    Icons.search,
                    color: Colors.red,
                    size: 32.0,
                  ),
                ),
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
                  height: MediaQuery.of(context).size.height / 3,
                  child: BlocBuilder<DiscoverMovieBloc, DiscovermovieState>(
                      builder: (context, state) {
                    if (state is DiscoverMovieInitial) {
                      return buildInitialInput();
                    } else if (state is DiscoverMovieLoading) {
                      return buildLoading();
                    } else if (state is DiscoverMovieLoaded) {
                      return buildBanner(context, state.discoverMovies);
                    } else {
                      return buildInitialInput();
                    }
                  }),
                ),
                Container(
                  padding: EdgeInsets.only(left: 5.0),
                  height: 61,
                  child: BlocBuilder<GenreBloc, GenreState>(
                    builder: (context, state) {
                      if (state is GenreInitial) {
                        return buildInitialInput();
                      } else if (state is GenreLoading) {
                        return buildLoading();
                      } else if (state is GenreLoaded) {
                        return buildLoadedGenreData(
                            context, state.genreResponse);
                      } else {
                        return buildInitialInput();
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                SectionContainer(
                  sectionTitle: 'My List',
                  child: Container(
                    height: MediaQuery.of(context).size.height / 3,
                    child: BlocBuilder<FeaturedMovieBloc, FeaturedMovieState>(
                      builder: (context, state) {
                        if (state is FeaturedMovieInitial) {
                          return buildInitialInput();
                        } else if (state is FeaturedMovieLoading) {
                          return buildLoading();
                        } else if (state is FeaturedMovieLoaded) {
                          return buildListData(
                              context, state.featuredMovieResponse);
                        } else {
                          return buildInitialInput();
                        }
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: 15.0,
                ),
                SectionContainer(
                  sectionTitle: 'Most Popular',
                  child: Container(
                    height: MediaQuery.of(context).size.height / 3,
                    child: BlocBuilder<FeaturedMovieBloc, FeaturedMovieState>(
                      builder: (context, state) {
                        if (state is FeaturedMovieInitial) {
                          return buildInitialInput();
                        } else if (state is FeaturedMovieLoading) {
                          return buildLoading();
                        } else if (state is FeaturedMovieLoaded) {
                          return buildListData(
                              context, state.featuredMovieResponse);
                        } else {
                          return buildInitialInput();
                        }
                      },
                    ),
                  ),
                ),
              ],
            ),
          )),
    );
  }

  Widget buildInitialInput() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

/*  Widget buildLoadedData(
      BuildContext context, FeaturedMovieResponse featuredMovieResponse) {
    print(featuredMovieResponse.toString());
    if (featuredMovieResponse.results.length > 0) {
      return HomePageFeaturedWidget(featuredMovieResponse.results);
    } else {
      return Text("No data available");
    }
  }*/

  Widget buildBanner(BuildContext context, DiscoverMovies discoverMovies) {
    print(discoverMovies.toString());
    if (discoverMovies.results.length > 0) {
      return HomePageFeaturedWidget(discoverMovies.results);
    } else {
      return Text("No data available");
    }
  }

  Widget buildListData(
      BuildContext context, FeaturedMovieResponse featuredMovieResponse) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      itemCount: featuredMovieResponse.results.length,
      itemBuilder: (ctx, id) {
        return MovieContainer(
          snapshot: featuredMovieResponse.results[id],
        );
      },
    );
  }

  Widget buildLoadedGenreData(
      BuildContext context, GenreResponse genreResponse) {
    if (genreResponse.genres.length > 0) {
      return ListView.builder(
          itemCount: genreResponse.genres.length,
          scrollDirection: Axis.horizontal,
          itemBuilder: (ctx, id) {
            return GenreContainer(
              genre: genreResponse.genres[id],
            );
          });
    } else {
      return Text("No data available");
    }
  }
}
