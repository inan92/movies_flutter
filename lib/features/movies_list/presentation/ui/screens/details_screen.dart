import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:fluttermoviesapp/core/util/constants.dart';
import 'package:fluttermoviesapp/core/util/global.dart';
import 'package:fluttermoviesapp/features/movie_details/data/models/movie_details.dart';
import 'package:fluttermoviesapp/features/movie_details/presentation/bloc/detailsbloc/bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/ui/widgets/my_hero.dart';

class DetailsScreen extends StatefulWidget {
  final int id;

  const DetailsScreen({Key key, this.id}) : super(key: key);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  MovieDetailsBloc movieDetailsBloc;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    movieDetailsBloc = BlocProvider.of<MovieDetailsBloc>(context)
      ..add(GetMovieDetails(widget.id, API_KEY));
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: BlocBuilder<MovieDetailsBloc, MovieDetailsState>(
          bloc: movieDetailsBloc,
          builder: (context, state) {
            print(state);
            if (state is MovieDetailInitial) {
              return buildInitialInput();
            } else if (state is MovieDetailLoading) {
              return buildLoading();
            } else if (state is MovieDetailsLoaded) {
              return buildLoadedLayout(context, state.movieDetailsResponse);
            } else if (state is MovieDetailsError) {
              return buildErrorOutput(state.errorMessage);
            }else{
              return buildInitialInput();
            }
          },
        ),
      ),
    );
  }

  Widget buildInitialInput() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildErrorOutput(String error) {
    return Center(
      child: Text(error),
    );
  }

  Widget buildLoading() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }

  Widget buildLoadedLayout(
      BuildContext context, MovieDetailsResponse snapshot) {
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          MyHero(
            imgUrl: getPosterImage(snapshot.posterPath),
          ),
          SizedBox(
            height: 11,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0),
            child: Column(
              children: <Widget>[
                Text(
                  "${snapshot.originalTitle}",
                  style: Theme.of(context).textTheme.headline,
                ),
                SizedBox(
                  height: 7.0,
                ),
                RichText(
                  text: TextSpan(
                    children: List.generate(
                      snapshot.genres.length,
                      (i) {
                        return TextSpan(text: "${snapshot.genres[i].name} ");
                      },
                    ),
                    style: Theme.of(context).textTheme.caption,
                  ),
                ),
                SizedBox(height: 9.0),
                RatingBar(
                  initialRating: snapshot.voteAverage,
                  direction: Axis.horizontal,
                  allowHalfRating: true,
                  itemCount: 10,
                  itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) => Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  itemSize: 25,
                  onRatingUpdate: (rating) {
                    print(rating);
                  },
                ),
                SizedBox(height: 13.0),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                          "Year",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(
                          "${DateTime.parse(snapshot.releaseDate).year}",
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "Language",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(
                          "${snapshot.originalLanguage}",
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          "Length",
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Text(
                          "${snapshot.runtime} min",
                          style: Theme.of(context).textTheme.subhead,
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: 13.0),
                Text(
                  "${snapshot.overview}",
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .body1
                      .apply(fontSizeFactor: 1.2),
                ),
                SizedBox(height: 13.0),
              ],
            ),
          ),
          // MyScreenshots(),
          SizedBox(height: 13.0),
        ],
      ),
    );
  }
}
