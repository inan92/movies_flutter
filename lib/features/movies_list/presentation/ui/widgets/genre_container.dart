import 'package:flutter/material.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/genres.dart';

class GenreContainer extends StatelessWidget {
  final Genres genre;

  const GenreContainer({
    Key key,
    this.genre,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(15.0),
        color: Colors.red,
        boxShadow: [
          BoxShadow(
            color: Colors.red,
            blurRadius: 2.5,
          )
        ],
      ),
      // width: MediaQuery.of(context).size.width / 2.5,
      constraints: BoxConstraints(minWidth: 150),
      alignment: Alignment.center,
      margin: EdgeInsets.symmetric(
        horizontal: 7,
        vertical: 5.0,
      ),
      padding: const EdgeInsets.symmetric(horizontal: 5.0),
      child: Text(
        "${genre.name}",
        style: Theme.of(context).textTheme.headline.apply(color: Colors.white),
      ),
    );
  }
}
