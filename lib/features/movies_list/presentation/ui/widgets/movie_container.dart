import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttermoviesapp/core/rest/movie_service.dart';
import 'package:fluttermoviesapp/core/util/global.dart';
import 'package:fluttermoviesapp/features/movie_details/data/repositories/movie_details_repository_impl.dart';
import 'package:fluttermoviesapp/features/movie_details/presentation/bloc/detailsbloc/movie_details_bloc.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/featured_movies.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/ui/screens/details_screen.dart';

class MovieContainer extends StatelessWidget {
  final Results snapshot;

  const MovieContainer({
    Key key,
    this.snapshot,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => BlocProvider<MovieDetailsBloc>(
              create: (BuildContext context) => MovieDetailsBloc(
                  MovieDetailsRepositoryImpl(MovieApiService.create()),
                  this.snapshot.id),
              child: DetailsScreen(
                id: this.snapshot.id,
              ),
            ),
          ),
        );
      },
      child: Container(
        width: MediaQuery.of(context).size.width / 2.5,
        margin: EdgeInsets.fromLTRB(0, 15, 15, 15),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(15.0),
          boxShadow: [
            BoxShadow(
                blurRadius: 5.0, color: Colors.grey[400], offset: Offset(0, 3))
          ],
        ),
        child: Stack(
          children: <Widget>[
            Positioned.fill(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(15.0),
                child: Image.network(
                  getPosterImage(snapshot.posterPath),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Container(
                padding: EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(15),
                    bottomRight: Radius.circular(15),
                  ),
                  color: Colors.black45,
                ),
                child: Text(
                  snapshot.originalTitle,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                  softWrap: true,
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
