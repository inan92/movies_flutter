import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttermoviesapp/core/rest/movie_service.dart';
import 'package:fluttermoviesapp/core/util/global.dart';
import 'package:fluttermoviesapp/features/movie_details/data/repositories/movie_details_repository_impl.dart';
import 'package:fluttermoviesapp/features/movie_details/presentation/bloc/detailsbloc/movie_details_bloc.dart';
import 'package:fluttermoviesapp/features/movie_discover/data/models/discover_movies.dart';
import 'package:fluttermoviesapp/features/movies_list/presentation/ui/screens/details_screen.dart';

class HomePageFeaturedWidget extends StatelessWidget {
  List<ResultsBean> featuredMovieList;

  HomePageFeaturedWidget(this.featuredMovieList);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return PageView.builder(
      itemCount: featuredMovieList.length,
      itemBuilder: (ctx, id) {
        print(featuredMovieList[id].toString());
        return GestureDetector(
          onTap: (){
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) => BlocProvider<MovieDetailsBloc>(
                  create: (BuildContext context) => MovieDetailsBloc(
                      MovieDetailsRepositoryImpl(MovieApiService.create()),
                      this.featuredMovieList[id].id),
                  child: DetailsScreen(
                    id: this.featuredMovieList[id].id,
                  ),
                ),
              ),
            );
          },
          child: Container(
            margin: EdgeInsets.all(15.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15.0),
              boxShadow: [
                BoxShadow(
                    blurRadius: 5.0,
                    color: Colors.grey[400],
                    offset: Offset(0, 3))
              ],
            ),
            child: Stack(
              children: <Widget>[
                Positioned.fill(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: Image.network(
                      // movieList[id]['img'],
                      getPosterImage(featuredMovieList[id].posterPath),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(15),
                        bottomRight: Radius.circular(15),
                      ),
                      color: Colors.black45,
                    ),
                    child: Text(
                      "${featuredMovieList[id].title}",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 18,
                      ),
                      softWrap: true,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
