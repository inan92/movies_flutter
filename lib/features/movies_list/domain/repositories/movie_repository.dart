import 'package:chopper/chopper.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/featured_movies.dart';
import 'package:fluttermoviesapp/features/movies_list/data/models/genres.dart';

abstract class MovieRepository {
  Future<Response> getFeatureMovies(String apiKey);

  Future<Response> getGenreList(String apiKey);
}
