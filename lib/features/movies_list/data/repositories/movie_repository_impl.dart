import 'package:chopper/src/response.dart';
import 'package:fluttermoviesapp/core/rest/movie_service.dart';
import 'package:fluttermoviesapp/features/movies_list/domain/repositories/movie_repository.dart';

class MovieRepositoryImpl implements MovieRepository {
  final MovieApiService mService;

  MovieRepositoryImpl(this.mService);

  @override
  Future<Response> getFeatureMovies(String apiKey) {
    // TODO: implement getFeatureMovies
    return mService.getFeaturedMovies(apiKey);
  }

  @override
  Future<Response> getGenreList(String apiKey) {
    // TODO: implement getGenreList
    return mService.getGenres(apiKey);
  }
}
